//Michael Knyszek, Baseball Program

#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <limits>
#include <queue>

using namespace std;

struct E {
	int c; //capacity
	int f; //flow
	int to; //index of connecting vertex
	explicit E(int c, int f, int to): c(c), f(f), to(to) {};
};

struct V {
	int p;
	bool source;
	bool sink;
	int team;
	vector<E> e; //List of edges to adjecent vertices
	explicit V(): source(false), sink(false), e(vector<E>()), team(-1) {};
};

//Returns constructed graph for nontrivial elimination
vector<V> construct_graph(const vector<vector<int> >& g, const vector<int>& w, const vector<int>& r, const int& x, const int& n) { 
	vector<V> flow;
	V source = V();
	source.source = true;
	V sink = V();
	sink.sink = true;
	flow.push_back(source);

	int N = n-1;

	vector<V> teams(n);
	for(int i = 0; i < n; ++i) {
		teams[i] = V();
		teams[i].team = i;
	}
	int size = 2 + N + ((N-1)*(N-1) + (N-1))/2;
	int total = 1;
	for(int i = 0; i < N; ++i) {
		for(int j = i+1; j < N; j++) {
			V game = V();
			int proto_i = i, proto_j = j;
			if(i >= x) proto_i = i+1;
			if(j >= x) proto_j = j+1;
			flow[0].e.push_back(E(g[proto_i][proto_j], 0, total));
			++total;
			game.e.push_back(E(numeric_limits<int>::max(), 0, size-1-N+i));
			game.e.push_back(E(numeric_limits<int>::max(), 0, size-1-N+j));
			flow.push_back(game);
		}
	}

	for(int i = 0; i < n; ++i) {
		if(teams[i].team != x) {
			teams[i].e.push_back(E(w[x] + r[x] - w[i], 0, size-1));
			flow.push_back(teams[i]);
		}
	}

	flow.push_back(sink);

	/*cout << "INITIAL FLOWS" << endl;
	for(int j = 0; j < flow[0].e.size(); ++j) cout << flow[0].e[j].to << ' ' << flow[0].e[j].f << ' ' << flow[0].e[j].c << endl;
	for(int i = 1; i < flow.size(); ++i) {
		for(int j = 0; j < flow[i].e.size(); ++j) {
			cout << i << ' ' << flow[i].e[j].to << ' ' << flow[i].e[j].f << ' ' << flow[i].e[j].c << endl;
		}
	}*/

	return flow;
}

int find_path(vector<V>& graph, vector<int>& parents) {
	vector<int> path_capacity(graph.size(), 0);
	parents[0] = -2;
	path_capacity[0] = numeric_limits<int>::max();
	queue<int> Q;
	Q.push(0);
	while(Q.size() > 0) {
		int u = Q.front(); 
		Q.pop();
		for(int i = 0; i < graph[u].e.size(); ++i) {
			if(graph[u].e[i].c - graph[u].e[i].f > 0 && parents[graph[u].e[i].to] == -1) {
				parents[graph[u].e[i].to] = u;
				path_capacity[graph[u].e[i].to] = min(path_capacity[u], graph[u].e[i].c - graph[u].e[i].f);
				if(graph[u].e[i].to == graph.size()-1) {
					return path_capacity[graph.size()-1];
				}
				Q.push(graph[u].e[i].to);		
			}
		}
	}
	return 0;
}

int main() {
    ofstream fout ("baseball.out");
    ifstream fin ("baseball.in");

    int N;
    fin >> N;
    
    vector<string> n(N);
    vector<int> w(N);
    vector<int> l(N);
    vector<int> r(N);
    vector<vector<int> > g(N);

    //Input
    for(int i = 0; i < N; ++i) {
    	fin >> n[i] >> w[i] >> l[i] >> r[i];
    	g[i] = vector<int>(N);
    	for(int j = 0; j < N; ++j) fin >> g[i][j];
    }

	for(int i = 0; i < N; ++i) {
		vector<string> R;
		//Trivial Elimintation
		for(int j = 0; j < N; j++) {
			if(j != i) {
				if(w[i] + r[i] < w[j]) {
					R.push_back(n[j]);
					break;
				}
			}
		}

		//Non-trivial Elimination
		if(!R.size()) {
			//cout << "Non-trivial: " << n[i] << endl; 
			vector<V> graph = construct_graph(g, w, r, i, N);
			while(true) {
				vector<int> parents(graph.size(), -1);
				int path_capacity = find_path(graph, parents);
				if(!path_capacity) break;
				int v = graph.size()-1;
				while(v != 0) {
					int u = parents[v];
					for(int j = 0; j < graph[u].e.size(); ++j) {
						if(graph[u].e[j].to == v) {
							graph[u].e[j].f += path_capacity;
							break;
						}
					}
					v = u;
				}
			}

			/*cout << "FINAL FLOWS" << endl;
			for(int j = 0; j < graph[0].e.size(); ++j) cout << graph[0].e[j].to << ' ' << graph[0].e[j].f << ' ' << graph[0].e[j].c << endl;
			for(int i = 1; i < graph.size(); ++i) {
				for(int j = 0; j < graph[i].e.size(); ++j) {
					cout << i << ' ' << graph[i].e[j].to << ' ' << graph[i].e[j].f << ' ' << graph[i].e[j].c << endl;
				}
			}*/

			bool eliminated = false;
			for(int i = 0; i < graph[0].e.size(); ++i) {
				if(graph[0].e[i].f != graph[0].e[i].c) {
					eliminated = true;
				}
			}

			//Find teams in subset R based on graph, if eliminated.
			if(eliminated) {
				for(int j = graph.size()-N-2; j < graph.size()-1; ++j) {
					if(graph[j].e[0].f == graph[j].e[0].c) {
						R.push_back(n[graph[j].team]);
					}
				}
			}
		}

		//Print out result to file appropriately
		if(R.size()) {
			fout << n[i] << " is eliminated by the subset R = { ";
			for(int j = 0; j < R.size(); ++j) fout << R[j] << ' ';
			fout << "}" << endl;
		}else{
			fout << n[i] << " is not eliminated" << endl;
		}
		
	}
	return 0;
}