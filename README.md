# Baseball Elimination Project #

This project was one assigned in my Data Structures class in my senior year of high school. The problem statement was taken by my teacher at the time from a Princeton algorithms course, although my implementation here is in C++. 

http://www.cs.princeton.edu/courses/archive/spr03/cos226/assignments/baseball.html

The main purpose of the project was to implement the Ford-Fulkerson algorithm, but I also implemented my own solution to this problem using a DFS-based approach to fill the network.

### Compile & Run ###

* Compile: 
g++ -o baseball baseball_[dfs/ff].cpp

* Run: 
./baseball